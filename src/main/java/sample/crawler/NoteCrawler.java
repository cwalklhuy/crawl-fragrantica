package sample.crawler;

import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import sample.entity.Perfume;

import java.io.IOException;
import java.util.*;

public class NoteCrawler {

    public static String getThumbnail(Connection connection) throws IOException {
        Document document = connection.get();
        String cssQuery = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(1)";
        Element element = document.select(cssQuery).first();
        String style = element.attr("style");

        return style;
    }

    public static Set<Perfume> getLinkPerfumes(Connection connection) throws IOException {
        Document document = connection.get();
        //*[@id="main-content"]/div[1]/div[1]/div/div[4]/div/div[1]/div
        String cssQuery = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(4) > div > div.cell.medium-8.large-9.medium-order-2 > div > div";
        Elements elements = document.select(cssQuery);
        Iterator<Element> elementIterator = elements.iterator();
        Set<Perfume> perfumes = new HashSet<>();
        while (elementIterator.hasNext()) {
            Element element = elementIterator.next();
            Elements img = element.getElementsByTag("img");
            if (!img.isEmpty()) {
                Element elementImg = img.first();
                String linkImg = elementImg.attr("src");
                Element elementsByClass = element.getElementsByClass("flex-child-auto").first();
                String perfumeName = elementsByClass.text();
                Elements a = elementsByClass.getElementsByTag("a");
                String href = a.attr("href");
                Perfume perfume = Perfume.builder()
                        .name(perfumeName)
                        .imageLink(linkImg)
                        .linkPerfume(href)
                        .build();
                perfumes.add(perfume);
            }


        }

        return perfumes;
    }
}
