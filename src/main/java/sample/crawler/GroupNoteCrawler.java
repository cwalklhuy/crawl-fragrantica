package sample.crawler;

import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import sample.entity.Note;
import sample.entity.GroupNote;
import sample.jsoup.JsoupConnection;

import java.io.IOException;
import java.util.*;

public class GroupNoteCrawler {

    public List<GroupNote> getListNote(Connection connection) throws IOException {
        List<GroupNote> result = new ArrayList<>();
        String cssQuery = "a[href*=groupnotes_group]";
        Document document = connection.get();
        Elements elements = document.select(cssQuery);
        Iterator<Element> iterator = elements.iterator();
        List<String> listNoteName = new ArrayList<>();
        while (iterator.hasNext()) {
            Element element = iterator.next();
            listNoteName.add(element.text());
        }
        String xPath = "";
        //CITRUS SMELLS
        xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(6) > div > div";
        String noteName = listNoteName.get(0);
        Elements elements1 = document.select(xPath);
        Iterator<Element> elementIterator = elements1.iterator();
        Set<Note> groupNotes = new HashSet<>();
        while (elementIterator.hasNext()) {
            Element element = elementIterator.next();
            Note groupNote = getGroupNote(element);
            groupNotes.add(groupNote);
        }
        GroupNote note = GroupNote.builder()
                .name(noteName)
                .groupNotes(groupNotes)
                .build();

        result.add(note);
//        for (String e : listNoteName) {
//            switch (e) {
//                case "CITRUS SMELLS":
//                    ////*[@id="main-content"]/div[1]/div[1]/div/div[6]/div/div[1]
//                    //document.select("#docs > div:eq(1) > h4 > a").attr("href");
//                    xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(6) > div > div";
//                    break;
//                case "FRUITS, VEGETABLES AND NUTS":
//                    xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(9) > div > div";
//                    break;
//                case "FLOWERS":
//                    xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(12) > div > div";
//                    break;
//                case "WHITE FLOWERS":
//                    xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(15) > div > div";
//                    break;
//                case "GREENS, HERBS AND FOUGERES":
//                    xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(18) > div > div";
//                    break;
//                case "SPICES":
//                    xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(21) > div > div";
//                    break;
//                case "SWEETS AND GOURMAND SMELLS":
//                    xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(24) > div > div";
//                    break;
//                case "WOODS AND MOSSES":
//                    xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(27) > div > div";
//                    break;
//                case "RESINS AND BALSAMS":
//                    xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(30) > div > div";
//                    break;
//                case "MUSK, AMBER, ANIMALIC SMELLS":
//                    xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(33) > div > div";
//                    break;
//                case "BEVERAGES":
//                    xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(36) > div > div";
//                    break;
//                case "NATURAL AND SYNTHETIC, POPULAR AND WEIRD":
//                    xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(39) > div > div";
//                    break;
//
//            }
//            Elements elements1 = document.select(xPath);
//            Iterator<Element> elementIterator = elements1.iterator();
//            Set<GroupNote> groupNotes = new HashSet<>();
//            while(elementIterator.hasNext()) {
//                Element element = elementIterator.next();
//                String groupNoteName = element.getElementsByTag("a").text();
//                String groupNoteLink = element.getElementsByTag("a").attr("href");
//                String background = element.getElementsByTag("img").attr("src");
//                String thumbnail = GroupNoteCrawler.getThumbnail(new JsoupConnection(groupNoteLink).getConnection());
//                groupNotes.add(GroupNote.builder()
//                        .name(groupNoteName)
//                        .background(background)
//                        .thumbnail(thumbnail)
//                        .linkGroupNote(groupNoteLink)
//                        .build());
//                System.out.println("Done " + groupNoteName);
//            }
//            Note note = Note.builder()
//                    .name(e)
//                    .groupNotes(groupNotes)
//                    .build();
//
//            result.add(note);
//        }


        return result;
    }


    private Note getGroupNote(Element element) throws IOException {
        String groupNoteName = element.getElementsByTag("a").text();
        String groupNoteLink = element.getElementsByTag("a").attr("href");
        String background = element.getElementsByTag("img").attr("src");
        Connection groupNoteConnection = new JsoupConnection(groupNoteLink).getConnection();
        String thumbnail = NoteCrawler.getThumbnail(groupNoteConnection);

        return Note.builder()
                .name(groupNoteName)
                .background(background)
                .thumbnail(thumbnail)
                .linkNote(groupNoteLink)
                .build();
    }
}
