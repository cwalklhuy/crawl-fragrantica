package sample.crawler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import sample.entity.Note;
import sample.entity.GroupNote;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class Crawler {
    private WebDriver driver;
    private String mainUrl;

    public Crawler() {
        this.driver = new ChromeDriver();
    }

    public void setMainURL(String mainURL) {
        this.mainUrl = mainURL;
        this.driver.get(mainURL);
        this.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    public void close() {
        this.driver.quit();
    }

    public List<GroupNote> getMainLinks() {
        List<GroupNote> result = new ArrayList<>();
        List<WebElement> elements = driver.findElements(By.xpath("//div[@class='cell text-center gone4empty']/p/a"));
        elements.forEach(e -> {
            String noteName = e.getText().trim();
            GroupNote note = GroupNote.builder().name(noteName).build();
            String xPath = "";
            switch (noteName) {
                case "CITRUS SMELLS":
                    xPath = "//*[@id=\"main-content\"]/div[1]/div[1]/div/div[6]/div/div";
                    final Set<Note> groupNotes = getGroupNotesByXpath(xPath, driver);
                    note.setGroupNotes(groupNotes);
                    System.out.println(note.getName());
                    result.add(note);
                    break;
                case "FRUITS, VEGETABLES AND NUTS":
                    xPath = "//*[@id=\"main-content\"]/div[1]/div[1]/div/div[9]/div/div";
                    final Set<Note> groupNotes1 = getGroupNotesByXpath(xPath, driver);
                    note.setGroupNotes(groupNotes1);
                    System.out.println(note.getName());
                    result.add(note);
                    break;
//                case "FLOWERS":
//                    System.out.println("FLOWERS");
//                    break;
//                case "WHITE FLOWERS":
//                    System.out.println("WHITE FLOWERS");
//                    break;
//                case "GREENS, HERBS AND FOUGERES":
//                    System.out.println("GREENS, HERBS AND FOUGERES");
//                    break;
//                case "SPICES":
//                    System.out.println("SPICES");
//                    break;
//                case "SWEETS AND GOURMAND SMELLS":
//                    System.out.println("SWEETS AND GOURMAND SMELLS");
//                    break;
//                case "WOODS AND MOSSES":
//                    System.out.println("WOODS AND MOSSES");
//                    break;
//                case "RESINS AND BALSAMS":
//                    System.out.println("RESINS AND BALSAMS");
//                    break;
//                case "MUSK, AMBER, ANIMALIC SMELLS":
//                    System.out.println("MUSK, AMBER, ANIMALIC SMELLS");
//                    break;
//                case "BEVERAGES":
//                    System.out.println("BEVERAGES");
//                    break;
//                case "NATURAL AND SYNTHETIC, POPULAR AND WEIRD":
//                    System.out.println("NATURAL AND SYNTHETIC, POPULAR AND WEIRD");
//                    break;
            }

        });

        return result;
    }

    private Set<Note> getGroupNotesByXpath(String xPath, WebDriver driver) {
        List<WebElement> elements = driver.findElements(By.xpath(xPath));
        Set<Note> groupNotes = new HashSet<>();
        for (WebElement e : elements) {
            String name = e.getText().trim();
            String background = e.findElement(By.xpath("./a/img")).getAttribute("src");
            String link = e.findElement(By.xpath("a")).getAttribute("href");
            driver.get(link);
            String pageSource = driver.getPageSource();
            Document document = Jsoup.parse(pageSource);
            Element element = document.select("img.thumbnail").first();
            System.out.println(element.attr("src" + "Page 2"));
            groupNotes.add(Note.builder()
                    .name(name)
                    .background(background)
                    .build());
            driver.navigate().back();
        }

        return groupNotes;
    }
}
