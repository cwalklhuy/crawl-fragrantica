package sample.jsoup;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

public class JsoupConnection {
    private Connection connection;

    public JsoupConnection(String mainURL) {
        this.connection = Jsoup.connect(mainURL);
    }

    public Connection getConnection() {
        return connection;
    }
}
