package sample.entity;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Perfume {
    private int id;
    private String name;
    private double vote;
    private int voters;
    private String imageLink;
    private Longevity longevity;
    private Sillage sillage;
    private String linkPerfume;

    private Set<NotePerfume> notePerfumes;
}
