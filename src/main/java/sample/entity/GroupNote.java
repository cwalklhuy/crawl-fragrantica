package sample.entity;

import lombok.*;

import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GroupNote {
    private int id;
    private String name;
    private Set<Note> groupNotes;

}
