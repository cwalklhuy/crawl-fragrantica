package sample.entity;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Note {
    private int id;
    private String name;
    private GroupNote groupNote;
    private String background;
    private String thumbnail;
    private String linkNote;
    private Set<Image> images;
    private Set<NotePerfume> notePerfumes;
}
