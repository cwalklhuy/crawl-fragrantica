package sample.entity;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Longevity {
    private int id;
    private int poor;
    private int week;
    private int moderate;
    private int longLasting;
    private int veryLongLasting;
    private Set<Perfume> perfumes;
}
