package sample.entity;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Image {
    private int id;
    private String linkImage;
    private Note groupNote;
}
