package sample.entity;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NotePerfume {
    private Note note;
    private Perfume perfume;
    private int score;
}
