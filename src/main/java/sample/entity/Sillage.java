package sample.entity;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Sillage {
    private int id;
    private int soft;
    private int moderator;
    private int heavy;
    private int enormous;
    private Set<Perfume> perfumes;
}
