package sample.net;

import sample.constant.URLConstant;
import sample.crawler.NoteCrawler;
import sample.crawler.GroupNoteCrawler;
import sample.entity.GroupNote;
import sample.entity.Perfume;
import sample.jsoup.JsoupConnection;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public class Main {

    public static void main(String[] args) throws IOException {
        JsoupConnection jsoupConnection = new JsoupConnection(URLConstant.URL);
        GroupNoteCrawler crawlerJsoup = new GroupNoteCrawler();
        List<GroupNote> groupNotes = crawlerJsoup.getListNote(jsoupConnection.getConnection());
        for (GroupNote groupNote : groupNotes) {
            groupNote.getGroupNotes().forEach(e -> {
                String linkGroupNote = e.getLinkNote();
                try {
                    Set<Perfume> linkPerfumes = NoteCrawler.getLinkPerfumes(new JsoupConnection(linkGroupNote).getConnection());
                    linkPerfumes.forEach(perfume -> {
                        System.out.println(perfume.getImageLink() + " | " + perfume.getName());
                    });
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            });
        }
    }
}
